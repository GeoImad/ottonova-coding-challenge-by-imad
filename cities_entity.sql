/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE TABLE `cities_entity` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `continent` varchar(255) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL,
  `population` varchar(255) NOT NULL,
  `founded` varchar(255) NOT NULL,
  `landmarks` json NOT NULL,
  `name_native` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `cities_entity` (`id`, `name`, `country`, `continent`, `latitude`, `longitude`, `population`, `founded`, `landmarks`, `name_native`) VALUES
(1, 'Sydney', 'Australia', 'Australia', '-33.865143', '151.209900', '5312000', '1788', '[\"Sydney Opera House\", \"Sydney Harbour Bridge\", \"Queen Victoria Building\"]', 'Sydney');
INSERT INTO `cities_entity` (`id`, `name`, `country`, `continent`, `latitude`, `longitude`, `population`, `founded`, `landmarks`, `name_native`) VALUES
(2, 'New York City', 'USA', 'North America', '40.730610', '-73.935242', '8419000', '1624', '[\"Chrysler Building\", \"Brooklyn Bridge\", \"Madison Square Garden\"]', 'New York City');
INSERT INTO `cities_entity` (`id`, `name`, `country`, `continent`, `latitude`, `longitude`, `population`, `founded`, `landmarks`, `name_native`) VALUES
(6, 'Madrid', 'Spain', 'Europe', '40.416775', '-3.703790', '3223000', '1083', '[\"Royal Palace\", \"Plaza Mayor\", \"Plaza de Cibeles\"]', 'Madrid');
INSERT INTO `cities_entity` (`id`, `name`, `country`, `continent`, `latitude`, `longitude`, `population`, `founded`, `landmarks`, `name_native`) VALUES
(7, 'Moscow', 'Russia', 'Europe', '55.751244', '37.618423', '11920000', '1147', '[\"Saint Basil\'s Cathedral\", \"Kremlin\", \"Bolshoi Theatre\"]', 'Москва'),
(9, 'Tokyo', 'Japan', 'Asia', '35.652832', '139.839478', '13960000', '1603', '[\"Meji Shrine\", \"Asakusa\", \"Tokyo Skytree\"]', '東京'),
(10, 'Lagos', 'Nigeria', 'Africa', '6.465422', '3.406448', '14800000', '1914', '[\"Iga Idungaran\", \"Freedom Park\", \"The Cathedral Church of Christ\"]', 'Lagos'),
(11, 'Sao Paulo', 'Brazil', 'South America', '-23.533773', '-46.625290', '12330000', '1554', '[\"Mosteiro De Sao Bento\", \"Italian Building\", \"Farol Santander\"]', 'São Paulo'),
(12, 'Munich', 'Germany', 'Europe', '48.137154', '11.576124', '1472000', '1158', '[\"Bavaria statue\", \"Marienplatz\", \"ottonova office\"]', 'München');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;