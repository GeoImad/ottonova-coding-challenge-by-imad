# Ottonova Coding Challenge By Imad



## Getting started

In Controllers folder u will find cities controller and get all cities usecase 

## Entities and Services

Entities and Services are included in separeted folders

## Test 

E2e (End-to-end) test concerning Cities controller is in the same folder using jest (tests screenshots in the root repository)

## Database config

You will find the config in app module (also in cities.controller.spec.ts test file inside cities module) with {user: root, password: 779999} as credentials
Database named "test" and there is a sql file in the root project aswell, containing our table "Cities"
Tech used :
    - Mysql
    - TypeOrm
    - TablePLus (for handling database easily)