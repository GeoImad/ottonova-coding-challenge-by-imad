import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CitiesEntity } from '../entities/cities.entity';

@Injectable()
export class CitiesService {
  constructor(
    @InjectRepository(CitiesEntity)
    private citiesRepository: Repository<CitiesEntity>,
  ) { }

  findAll(): Promise<CitiesEntity[]> {
    return this.citiesRepository.find();
  }
}