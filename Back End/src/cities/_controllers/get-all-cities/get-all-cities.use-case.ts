import { Injectable } from "@nestjs/common";
import { CitiesService } from '../../services/cities.service';

@Injectable()
export class GetAllCitiesUseCase {
    constructor(private readonly citiesService: CitiesService) {}

    async run(): Promise<any> {
        const cities = await this.citiesService.findAll();
        return cities;
    }
}
