import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { CitiesModule } from '../cities.module';
import { CitiesService } from '../services/cities.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CitiesEntity } from '../entities/cities.entity';

describe('Cities', () => {
  let app: INestApplication;
  let citiesService: CitiesService;

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forFeature([CitiesEntity]),
        TypeOrmModule.forRoot({
          type: 'mysql',
          host: 'localhost',
          port: 3306,
          username: 'root',
          password: '779999',
          database: 'test',
          entities: [CitiesEntity],
          synchronize: true,
        }),
        CitiesModule],
    })
      .overrideProvider(CitiesService)
      .useValue(citiesService)
      .compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  it(`/GET cities`, async () => {
    const response = await request(app.getHttpServer())
      .get('/cities')
      .expect(200)    // testing if request is going OK

    expect(response.body).toHaveLength(8)   // testing if request is return same values as expected by comparing the length

    return response
  });

  afterAll(async () => {
    await app.close();
  });
});