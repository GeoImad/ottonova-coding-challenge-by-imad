import { Controller, Get } from "@nestjs/common";
import { GetAllCitiesUseCase } from "./get-all-cities/get-all-cities.use-case";

@Controller('cities')
export class CitiesController {
  constructor(
    private readonly getAllCitiesUseCase: GetAllCitiesUseCase,
  ) { }

  @Get()
  async findAll(): Promise<any> {
    return await this.getAllCitiesUseCase.run()
  }

}