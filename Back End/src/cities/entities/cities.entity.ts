import { Entity, Column, OneToMany, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class CitiesEntity {     // here we can identify columns, so we can provide relationships with external entities (manyToMany ...)

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name?: string;

  @Column()
  name_native?: string;

  @Column()
  country?: string;

  @Column()
  continent?: string;

  @Column()
  latitude?: string;

  @Column()
  longitude?: string;

  @Column()
  population?: string;

  @Column()
  founded?: string;

  @Column({ type: 'json' })
  landmarks?: string[];
}
