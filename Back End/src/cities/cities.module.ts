import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CitiesService } from './services/cities.service';
import { GetAllCitiesUseCase } from './_controllers/get-all-cities/get-all-cities.use-case';
import { CitiesController } from './_controllers/cities.controller';
import { CitiesEntity } from './entities/cities.entity';

const USE_CASES = [
    GetAllCitiesUseCase
];

const SERVICES = [
    CitiesService,
];


@Module({
    imports: [
        TypeOrmModule.forFeature([CitiesEntity])
    ],
    controllers: [CitiesController],
    providers: [...SERVICES, ...USE_CASES],
    exports: [
        ...SERVICES,
        GetAllCitiesUseCase
    ],
})
export class CitiesModule { }
