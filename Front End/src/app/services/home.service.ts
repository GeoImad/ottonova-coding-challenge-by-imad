import { Inject, Injectable, Injector } from '@angular/core';
import { from, BehaviorSubject, Observable, catchError, filter, map, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { City } from '../models/city.model';

@Injectable()
export class HomeService {
    private path: string = '';

    constructor(
        private http: HttpClient
    ) {
        this.path = `http://localhost:3000`;
    }

    getCities(
    ): Observable<City[]> {
        return this.http.get<any>(this.path + '/cities').pipe(
            map(response => response.map((res: City) => {
                return res
            }))
        );
    }

}
