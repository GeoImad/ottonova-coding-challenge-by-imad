import { trigger, state, style, AUTO_STYLE, transition, animate } from '@angular/animations';
import { Component, Input } from '@angular/core';
import { City } from 'src/app/models/city.model';

@Component({
    selector: 'city-card-component',
    templateUrl: './city-card.component.html',
    styleUrls: ['./city-card.component.css'],
    animations: [
        trigger('collapse', [
            state('false', style({ height: AUTO_STYLE, visibility: AUTO_STYLE })),
            state('true', style({ height: '0', visibility: 'hidden' })),
            transition('false => true', animate(300 + 'ms ease-in')),
            transition('true => false', animate(300 + 'ms ease-out'))
        ])
    ]
})
export class CityCardComponent {

    @Input() city!: City

    @Input() set expandFromHome(value: boolean) {
        if (value == true) { this.expand() } else { this.collapse()}
    }

    collapsed = true;

    toggle() {
        this.collapsed = !this.collapsed;
    }

    expand() {
        this.collapsed = false;
    }

    collapse() {
        this.collapsed = true;
    }
}
