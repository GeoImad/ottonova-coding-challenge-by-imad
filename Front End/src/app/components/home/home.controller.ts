import { Component, Injector, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { HomeService } from 'src/app/services/home.service';
import { City } from '../../models/city.model';

@Component({
    selector: 'home-controller',
    template: `
        <home-component
            [_cities]="cities"
            [loading]="loading"
        ></home-component>
    `,
})
export class HomeController
    implements OnInit, OnDestroy {

    cities!: City[]

    loading: boolean = true;

    constructor(
        private homeService: HomeService,
    ) { }

    ngOnInit(): void {
        this.homeService.getCities().subscribe((result) => {
            console.log(result);
            this.cities = result
            setTimeout(() => {
                this.loading = false
            }, 2000)

        })
    }

    ngOnDestroy(): void {
    }
}
