import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
import { HomeController } from './home.controller';
import { HomeRoutingModule } from './home.routing';
import { HomeService } from 'src/app/services/home.service';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { CityCardComponent } from '../cityCard/city-card.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    imports: [HomeRoutingModule, HttpClientModule, CommonModule, MatButtonModule, MatIconModule, NgbModule],
    declarations: [HomeComponent, HomeController, CityCardComponent],
    exports: [],
    providers: [HomeService],
})
export class HomeModule { }
