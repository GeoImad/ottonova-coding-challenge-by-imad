import { Component, Input, OnInit } from '@angular/core';
import { City } from 'src/app/models/city.model';

@Component({
  selector: 'home-component',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent {

cities!: City[]

noData!: boolean;

@Input() set _cities(value: any[]) {
    value.map((item) => {
        item.landmarks = item.landmarks.join(", ");
    })
    this.cities = value;
    if (value.length == 0) {
        this.noData = true
    }
}

@Input() loading!: boolean

expand: boolean = false

expandAll() {
    this.expand = !this.expand
}


}
